# My Dotfiles

## Fonts

 * Niarudi and Tycho: [kakwafont](https://github.com/kakwa/kakwafont) and [Uni 05_x](https://www.dafont.com/uni-05-x.font).
 * Nidhiki: [Terminus](https://packages.gentoo.org/packages/media-fonts/terminus-font)
